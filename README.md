# Java modules POPECO – user manual 2023

## Preparation
**Install Java** 

1.	Navigate to https://jdk.java.net/21 

<img src="img/install-java-1.png">

2.	Right-click the zip file for Windows/x64 (if you are using a modern PC) and download it to your PC. It will end up in your Downloads folder or in another recently used download location.

3.	Unzip the file. I personally unzipped to C:\ That is easy to find. But it does not matter where you unzip it.

4.	Check that you have a directory called _openjdk-21-ea+9_windows-x64_bin_ on your C drive (or in another preferred place).

<img src="img/install-java-2.png">

5.	You are ready to use OpenJDK

_It is advisable to delete the folders and downloaded software when the course has finished._

## Download the course modules

1.	Right-click with your mouse the link below to download popeco-learning-modules-1.0.zip

Download: [popeco-learning-modules-1.0.zip](https://git.wur.nl/popeco/learning-modules/-/raw/master/popeco-learning-modules-1.0.zip?inline=false)

This will download the zip file to your downloads folder or another folder recently used to download files.

2.	Unzip the file. I personally unzipped to C:\ Within the folder you should find nine modules. We use all of them except the lewontin module.

## Run the course modules (the easy way)
Double-click the modules to run them. For further instructions, see the documentation below.


## Run the course modules (the hard way - via dos prompt)

You can also run the modules from the command prompt which you find by typing `cmd` in the window that comes up if you click the Looking glass window next to the Windows button in the bottom-left of your screen.

<img src="img/manual-prompt.png">

Type in the search box: 
`cmd`
and press \<Enter\>

Then type the following things:
`M:\> C:`
(this brings you to the C drive)
`C:\> cd openjdk-21-ea+9_windows-x64_bin\jdk-21\bin`
(this is the folder where the executable program is located)

Then, run the module by typing: 
`C:\openjdk-21-ea+9_windows-x64_bin\jdk-21\bin> java -jar C:\popeco-learning-modules-1.0\popeco-leslie-1.0.jar`

This starts the Java module opens a little window:

<img src="img/manual-cells-window.png">

Click “Launch Cells”.

This starts the module. The user interface of the module comprises seven tabs. You may have to resize the window by clicking the square in the top right hand of the window to see the tabs on the lower side of the window. 

<img src="img/manual-resize.png">

It should look as follows:

<img src="img/manual-cells.png">

The tabs are (from left to right): Model, Change values, Numerical results, Graph of $`N \times t`$, Graph of $`log(N) \times t`$, Graph of $`N_1(t) \times N_2(t)`$ (phaseplane), Bar chart of $`N_1`$ and $`N_2`$.

From here, we can use the following instructions.

---
The package contains the following modules:

1. [Leslie](modules/leslie.md)
2. [Logist](modules/logist.md)
3. [Chaos](modules/chaos.md)
4. [Cells](modules/cells.md)
5. [Witcom](modules/witcom.md)
6. [Predator](modules/predator.md)
7. [Space1](modules/space1.md)
8. [Space2](modules/space2.md)
9. [Lewontin](modules/lewontin.md)

***
