## Lewontin

Evolutionary life cycle analyses of five species based on the Lewontin triangle.

Run: `D:\POPECO-Java> java -jar popeco-lewontin-1.0.jar`

---
This module presents Lewontin's (1965) analysis of the relationship between population growth rate and life history traits. Population growth is characterized by the relative rate of increase ($`r_m`$)

Lewontin represented age dependent reproduction as a simple triangle. The triangle gives values of $`R(x) = l(x) * m(x)`$ as a function of age.
The triangle is defined by four parameters:
- **$`A`$** - age at first reproduction (left corner of triangle)
- **$`H`$** - age at which reproduction peaks
- **$`R(H)`$** - peak reproducive rate (top of the triangle)
- **$`W`$** - age at which reproduction stops (right corner)

Lifetime reproduction $`R_0`$ is equal to the area of the triangle.

The module provides analysis for six species: two fruit flies _(Drosophila)_, a predatory roptifier _(Asplanchna)_, a beetle _(Tribolium)_, man _(Homo sapiens)_, and greenhouse whitefly _(Trialeurodes vaporariorum)_.

For each species the sensitivity of $`r_m`$ to changes in life history parameters is investigated.

[return to all modules page](../README.md)